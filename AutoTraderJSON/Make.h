//
//  Make.h
//  AutoTraderJSON
//
//  Created by iBuildX on 26/04/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface Make : JSONModel


@property (nonatomic) NSString <Optional> *label;
@property (nonatomic) NSString <Optional> *value;


@end
//{"label":"Acura","value":"ACURA" }
