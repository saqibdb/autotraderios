//
//  Utility.h
//  OpenWorkr
//
//  Created by iBuildx_Mac_Mini on 12/15/16.
//  Copyright © 2016 Eden. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UserNotifications/UserNotifications.h>


typedef void (^APIRequestResponseBlock) (id object, BOOL status, NSError *error);//block as a typedef
typedef void (^APIProgressResponseBlock) (NSProgress *progress);//block as a typedef

typedef void(^addressCompletion)(NSString *);

@interface Utility : NSObject< UNUserNotificationCenterDelegate>






+(void)getAutoTraderDateFromUrl :(NSString *)searchUrl  AndResponseBlock:(APIRequestResponseBlock)responseBlock AndProgressBlock:(APIProgressResponseBlock)progressBlock;


+(void)saveCarWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock;


+(void)loginWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock ;


+(void)loginPostManWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock;
+(void)getUserForMapWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock;
+(void)updateUserWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock;
+(void)uploadProfilePictureWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock;
+(void)getFavoriteWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock;
+(void)addFavoriteWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock ;


  
@end
