//
//  DetailViewController.m
//  AutoTraderJSON
//
//  Created by iBuildX on 25/04/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import "DetailViewController.h"
#import <SwiftSpinner/SwiftSpinner-Swift.h>

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSString *requestStr = [NSString stringWithFormat:@"https://www.autotrader.com/%@",self.selectedCar.vdpSeoUrl];
    NSLog(@"______%@" , requestStr);
    NSURL *url = [[NSURL alloc] initWithString:requestStr];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    [self.mainWebView loadRequest:request];
    
    self.mainWebView.navigationDelegate = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [SwiftSpinner show:@"Loading..." animated:YES];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation{
    
    NSLog(@"LOADED...");
    dispatch_async(dispatch_get_main_queue(), ^{
        [SwiftSpinner hide:nil];
    });
}

- (void)webView:(WKWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error{
    NSLog(@"FAILED...%@" , error.description);
    dispatch_async(dispatch_get_main_queue(), ^{
        [SwiftSpinner hide:nil];
    });
}
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(null_unspecified WKNavigation *)navigation{
    NSLog(@"STARTED...");
}
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error{
    NSLog(@"FAILED didFailProvisionalNavigation...%@" , error.description);
    dispatch_async(dispatch_get_main_queue(), ^{
        [SwiftSpinner hide:nil];
    });
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)closeAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
