//
//  MainTableViewCell.h
//  AutoTraderJSON
//
//  Created by iBuildX on 25/04/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CCMBorderView/CCMBorderView.h>

@interface MainTableViewCell : UITableViewCell



@property (weak, nonatomic) IBOutlet UILabel *titleText;
@property (weak, nonatomic) IBOutlet UILabel *trimText;

@property (nonatomic) IBOutlet CCMBorderView *mainBorderView;


@property (weak, nonatomic) IBOutlet UILabel *makeText;
@property (weak, nonatomic) IBOutlet UILabel *modelText;

@property (weak, nonatomic) IBOutlet UILabel *emailText;
@property (weak, nonatomic) IBOutlet UILabel *phonetext;

@property (weak, nonatomic) IBOutlet UILabel *newlyListedText;


@property (weak, nonatomic) IBOutlet CCMBorderView *topIndicatorView;
@property (weak, nonatomic) IBOutlet CCMBorderView *bottomIndicatorView;

@property (weak, nonatomic) IBOutlet UILabel *priceTxt;


@property (weak, nonatomic) IBOutlet UILabel *mileAgeTxt;



@end
