//
//  CarsCollectionViewCell.h
//  AutoTraderJSON
//
//  Created by iBuildX on 30/04/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CCMBorderView/CCMBorderView.h>
#import "AutoTraderJSON-Swift.h"



@interface CarsCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *makeTxt;

@property (weak, nonatomic) IBOutlet UILabel *modelTxt;


@property (weak, nonatomic) IBOutlet UILabel *allTxt;

@property (weak, nonatomic) IBOutlet CCMBorderView *backBorderView;

@property (weak, nonatomic) IBOutlet CCMBorderView *addView;





@end
