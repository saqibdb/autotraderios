//
//  CarSearch.h
//  AutoTraderJSON
//
//  Created by iBuildX on 30/04/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface CarSearch : JSONModel
@property (nonatomic) NSString <Optional> *carMake;
@property (nonatomic) NSString <Optional> *carModel;
@property (nonatomic) NSString <Optional> *carMakeCode;
@property (nonatomic) NSString <Optional> *carModelCode;



@property (nonatomic) NSString <Optional> *startYear;
@property (nonatomic) NSString <Optional> *endYear;


@property (nonatomic) NSString <Optional> *totalMatches;


@property (nonatomic) NSMutableArray *allOriginalSearchedCars;


@property (nonatomic) BOOL isFirstTime;

@property (nonatomic) BOOL isFirstTimeAfterTotal;

@property (nonatomic) BOOL haveTotal;



@end
