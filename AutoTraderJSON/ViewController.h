//
//  ViewController.h
//  AutoTraderJSON
//
//  Created by iBuildX on 25/04/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AKPickerView/AKPickerView.h>
#import "CarSearch.h"
#import <WebKit/WebKit.h>



@interface ViewController : UIViewController<UITableViewDataSource , UITableViewDelegate, UIPickerViewDataSource , UIPickerViewDelegate , UISearchBarDelegate, AKPickerViewDataSource, AKPickerViewDelegate , UICollectionViewDelegate , UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UIProgressView *mainProgressView;

@property (weak, nonatomic) IBOutlet UITableView *mainTableView;

@property (weak, nonatomic) IBOutlet UIPickerView *makePickerView;

@property (weak, nonatomic) IBOutlet UIPickerView *modelPickerView;
@property (weak, nonatomic) IBOutlet UIPickerView *endYear;
@property (weak, nonatomic) IBOutlet UIPickerView *startYear;


@property (weak, nonatomic) IBOutlet UILabel *totalText;

@property (weak, nonatomic) IBOutlet UISearchBar *carSearchBar;




@property (weak, nonatomic) IBOutlet UIView *modelLoderView;


@property (weak, nonatomic) IBOutlet AKPickerView *maxPagesPickerView;



@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;



@property (weak, nonatomic) IBOutlet UICollectionView *carsCollectionView;

@property (weak, nonatomic) IBOutlet AKPickerView *timePicker;


@property (weak, nonatomic) IBOutlet WKWebView *mainWebView;




- (IBAction)buttonAction:(UIButton *)sender;

- (IBAction)sliderButtonAction:(UIButton *)sender;
- (IBAction)dragAction:(UIButton *)sender;

- (IBAction)setAction:(UIButton *)sender;



@end

