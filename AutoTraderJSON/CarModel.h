//
//  CarModel.h
//  AutoTraderJSON
//
//  Created by iBuildX on 25/04/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface CarModel : JSONModel


@property (nonatomic) NSString <Optional> *title;
@property (nonatomic) NSString <Optional> *vin;
@property (nonatomic) NSNumber <Optional> *id;
@property (nonatomic) NSNumber <Optional> *newlyListed;
@property (nonatomic) NSString <Optional> *vdpSeoUrl;

@property (nonatomic) NSNumber <Optional> *isAlreadyOpened;

@property (nonatomic) NSNumber <Optional> *isVeryNew;


@property (nonatomic) NSNumber <Optional> *isReducedPriceDummy;



@property (nonatomic) NSString <Optional> *makeCode;
@property (nonatomic) NSString <Optional> *model;
@property (nonatomic) NSString <Optional> *ownerPhone;
@property (nonatomic) NSString <Optional> *contactEmail;
@property (nonatomic) NSString <Optional> *ownerName;



@property (nonatomic) NSString <Optional> *maxMileage;
@property (nonatomic) NSString <Optional> *derivedPrice;
@property (nonatomic) NSString <Optional> *salePrice;




@end
/*
 "accelerate": true,
 "certifiedTiles": [
 {
 "epn": "CERT",
 "image": {
 "src": "//images.autotrader.com/cms/images/na/certified/cpo/248500.jpg"
 },
 "link": {
 "href": "https://ad.doubleclick.net/ddm/trackclk/N3016.autotrader.comSD8257/B20573483.211919735;dc_trk_aid=410931956;dc_trk_cid=80562171;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=",
 "label": "Ford Certified Pre-Owned",
 "offsite": false
 },
 "thirdPartyImpression": "https://ad.doubleclick.net/ddm/trackimp/N3016.autotrader.comSD8257/B20573483.211919735;dc_trk_aid=410931953;dc_trk_cid=80561045;ord=@@ord@@1540828507516",
 "tileType": "CERTIFIED_CAR"
 }
 ],
 "description": {
 "hasMore": true,
 "label": "1 OWNER, CLEAN CARFAX HISTORY, FORD CERTIFIED, F-150 XLT, 4D SuperCrew, 5.0L V8, 10-Speed Automatic, 4WD, ABS brakes, Brake assist, Bumpers:..."
 },
 "hasSpecialOffer": false,
 "id": 492036801,
 "images": {
 "hasVideoIcon": false,
 "primary": 0,
 "sources": [
 {
 "alt": "Certified 2018 Ford F150 XLT - 492036801",
 "src": "https://images.autotrader.com/hn/c/17a3f331506a41058b1a057668735aa4.jpg",
 "title": "Certified 2018 Ford F150 XLT"
 }
 ]
 },
 "isHot": false,
 "isReducedPrice": true,
 "type": "Certified",
 "makeCode": "FORD",
 "make": "Ford",
 "model": "F150",
 "owner": {
 "dealerDiff": {},
 "distanceFromSearch": 25.1,
 "email": "groutsos@levittownford.com",
 "id": 66756167,
 "name": "Levittown Ford",
 "phone": {
 "linkText": "Get this seller's phone number",
 "privateNumber": false,
 "value": "18552914896",
 "isVisible": true
 },
 "privateSeller": false
 },
 "pricingDetail": {
 "assetType": "INCLUDED",
 "callForPricing": false,
 "ctaLink": false,
 "derived": "$32,436",
 "first": "$32,436",
 "incentive": 32436,
 "msrp": 0,
 "noPriceLabel": "Contact Dealer For Price",
 "payments": {
 "label": "Est. Finance Payment",
 "paymentDisclaimer": "Estimated payment based on 4.97% APR for 72 months with $4,865 down for well qualified buyers. Terms may vary.",
 "value": 463
 },
 "primary": 32436,
 "salePrice": 32436,
 "specialOffers": 0
 },
 "priority": "Premium",
 "productTiles": [
 {
 "epn": "carfaxVHR",
 "image": {
 "src": "/content/web/images/na/partner-tiles/255819.gif"
 },
 "link": {
 "href": "http://www.carfax.com/cfm/ccc_DisplayHistoryRpt.cfm?car_id=492036801&make=FORD&dealer_id=66756167&VIN=-108|-90|81|-87|18|-2|56|116|13|-15|-16|123|-29|63|109|36|-67|83|-64|83|53|-116|-45|89|&partner=ATD_X",
 "label": "View the Free CARFAX Report",
 "offsite": true
 },
 "tileType": "CARFAX_APPENDED"
 }
 ],
 "specifications": {
 "interiorColor": {
 "label": "Interior Color",
 "value": "Black"
 },
 "transmission": {
 "label": "Transmission",
 "value": "Automatic"
 },
 "mpg": {
 "label": "MPG",
 "value": "18 City / 23 Highway"
 },
 "engine": {
 "label": "Engine",
 "value": "8-Cylinder"
 },
 "driveType": {
 "label": "Drive Type",
 "value": "4 wheel drive - rear"
 },
 "mileage": {
 "label": "mile(s)",
 "value": "13,540"
 }
 },
 "style": [
 "Truck"
 ],
 "title": "Certified 2018 Ford F150 XLT",
 "vin": "1FTEW1E52JFB86914",
 "website": {
 "href": "/cars-for-sale/vehicledetails.xhtml?listingId=492036801&zip=10001&referrer=%2Fcars-for-sale%2Fsearchresults.xhtml%3Fzip%3D10001%26startYear%3D1981%26numRecords%3D100%26sortBy%3Drelevance%26incremental%3Dwithin_day%26firstRecord%3D0%26endYear%3D2019%26modelCodeList%3DF150PICKUP%26makeCodeList%3DFORD%26searchRadius%3D0&startYear=1981&numRecords=100&firstRecord=0&endYear=2019&modelCodeList=F150PICKUP&makeCodeList=FORD&searchRadius=0&makeCode1=FORD&modelCode1=F150PICKUP&digitalRetail=true&clickType=listing"
 },
 "year": 2018
 
 
 */
