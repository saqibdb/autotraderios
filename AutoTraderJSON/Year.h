//
//  Year.h
//  AutoTraderJSON
//
//  Created by iBuildX on 26/04/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface Year : JSONModel
@property (nonatomic) NSString <Optional> *name;
@property (nonatomic) NSString <Optional> *value;

@end
// "name": "2019",
//"value": "2019"
