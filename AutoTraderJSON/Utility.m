//
//  Utility.m
//  OpenWorkr
//
//  Created by iBuildx_Mac_Mini on 12/15/16.
//  Copyright © 2016 Eden. All rights reserved.
//

#import "Utility.h"
#import "SQBConstants.h"
#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@implementation Utility



+(void)getAutoTraderDateFromUrl :(NSString *)searchUrl  AndResponseBlock:(APIRequestResponseBlock)responseBlock AndProgressBlock:(APIProgressResponseBlock)progressBlock {
    /*
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    //manager.responseSerializer = [AFCompoundResponseSerializer serializer];
    NSString *userAgent = @"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36";
    [manager.requestSerializer setValue:userAgent forHTTPHeaderField:@"User-Agent"];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringCacheData];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    
    AFSecurityPolicy *securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    securityPolicy.allowInvalidCertificates = YES;
    [securityPolicy setValidatesDomainName:NO];
    manager.securityPolicy = securityPolicy;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];

    [manager.requestSerializer setValue:@"no-cache" forHTTPHeaderField:@"cache-control"];
    [manager.requestSerializer setValue:@"09d20996-31bc-4a8e-bf43-f4f019c9815b" forHTTPHeaderField:@"postman-token"];
    
    [manager GET:searchUrl parameters:nil headers:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        progressBlock(downloadProgress);
        NSLog(@"Data uploaded = %@" , downloadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        responseBlock(responseObject , YES , nil);

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        responseBlock(nil , NO , error);

    }];

*/
    /*[manager GET:searchUrl parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        progressBlock(downloadProgress);
        NSLog(@"Data uploaded = %@" , downloadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        responseBlock(responseObject , YES , nil);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        responseBlock(nil , NO , error);
    }];*/
    
    searchUrl = @"https://offerup.com/webapi/search/v2/feed/?q=Toyota&limit=1000&radius=50&platform=web&experiment_id=experimentmodel24&lon=-84.3879824&lat=33.7489954&sort=-posted";
    
    NSString* proxyHost = @"e2.fastpro.xyz";
    NSNumber* proxyPort = [NSNumber numberWithInt: 8080];
    NSString* userName = @"umer123.offerup_1";
    NSString* password = @"umer1234";

    // Create an NSURLSessionConfiguration that uses the proxy
    NSDictionary *proxyDict = @{@"HTTPSEnable" : [NSNumber numberWithInt:1],
                                (NSString *)kCFStreamPropertyHTTPSProxyHost : proxyHost,
                                (NSString *)kCFStreamPropertyHTTPSProxyPort : proxyPort,
                                (NSString *)kCFProxyUsernameKey : userName,
                                (NSString *)kCFProxyPasswordKey : password,
                                
                                @"HTTPEnable" : [NSNumber numberWithInt:1],
                                (NSString *)kCFStreamPropertyHTTPProxyHost : proxyHost,
                                (NSString *)kCFStreamPropertyHTTPProxyPort : proxyPort,
                                (NSString *)kCFProxyUsernameKey : userName,
                                (NSString *)kCFProxyPasswordKey : password,
                                
                                };
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    configuration.connectionProxyDictionary = proxyDict;
    
    configuration.requestCachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;

    
    
    
    
    
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"5bc2aa35-08c9-a436-905d-5d801e18feff" };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:searchUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    [request setValue:@"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36" forHTTPHeaderField:@"User-Agent"];

    


    
    //NSURLSession *session = [NSURLSession sharedSession];
    
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:[NSOperationQueue mainQueue]];

    
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString *str = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];

            
            NSLog(@"RESPONE = %@" , str);
            if (error) {
                responseBlock(nil , NO , error);
            } else {
                NSError *errorJson=nil;
                NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorJson];
                if(errorJson) {
                    responseBlock(nil , NO , errorJson);
                }
                else{
                    responseBlock(responseDict , YES , nil);
                }
            }
        });
    }];
    [dataTask resume];
    
    
    
    
}

+(void)saveCarWithDictionary :(NSDictionary *)dict  AndResponseBlock:(APIRequestResponseBlock)responseBlock {
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"postman-token": @"df936d2a-4730-bc09-bc53-98f4a790a56d" };
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://car-scrape-backend.herokuapp.com/save_car/"] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    [request setValue:@"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36" forHTTPHeaderField:@"User-Agent"];

    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"%@", error);
            responseBlock(nil , NO , error);
        } else {
            NSError* error;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSLog(@"json %@", json);
            responseBlock(json , YES , nil);
        }
    }];
    [dataTask resume];
}


@end
