//
//  DetailViewController.h
//  AutoTraderJSON
//
//  Created by iBuildX on 25/04/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import "CarModel.h"


@interface DetailViewController : UIViewController<WKNavigationDelegate>


@property (weak, nonatomic) IBOutlet WKWebView *mainWebView;


@property (nonatomic) CarModel *selectedCar;


- (IBAction)closeAction:(UIButton *)sender;

@end
