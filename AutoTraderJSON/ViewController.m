//
//  ViewController.m
//  AutoTraderJSON
//
//  Created by iBuildX on 25/04/2018.
//  Copyright © 2018 iBuildX. All rights reserved.
//

#import "ViewController.h"
#import "Utility.h"
#import "MainTableViewCell.h"
#import "CarModel.h"
#import "DetailViewController.h"
#import <SwiftSpinner/SwiftSpinner-Swift.h>
#import <TSMessages/TSMessageView.h>
#import "Make.h"
#import "Year.h"
#import <HexColors/HexColor.h>
#import <AudioToolbox/AudioToolbox.h>
#import "CarsCollectionViewCell.h"
#import "CarSearch.h"
#import "AutoTraderJSON-Swift.h"
#import "BackgroundTask.h"
#import <UserNotifications/UserNotifications.h>



@import Bugsnag;


@interface ViewController (){

    

    NSMutableArray *makeArray;
    NSMutableArray *yearsArray;
    NSMutableArray *modelArray;
    
    
    
    Make *selectedMake;
    Year *selectedModel;
    Year *selectedStart;
    Year *selectedEnd;

    __block BOOL haveTotal;
    __block int total;

    
    //NSTimer *mainTimer;
    
    
    NSArray *maxPagesArray;
    NSArray *timesArray;

    
    NSMutableArray *carSearches;
    
    int selectedSearch;
    
    __block int currentSearchingIndex;

    
    
    __block float selectedTime;
    
    
    
    
    
    
    __block int specialCounter;
    
    
    __block int specialErrorChecker;

}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    haveTotal = NO;
    currentSearchingIndex = 0;
    selectedTime = 20.0f;
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    
    
    specialCounter = 5;
    
    
    maxPagesArray = [NSArray arrayWithObjects:@"1000",@"1005", @"2000", @"3000" , @"5000" , @"10000", @"All", nil];
    timesArray = [NSArray arrayWithObjects:@"20 secs", @"30 secs", @"40 secs" , @"60 secs" , @"100 secs", nil];

    self.maxPagesPickerView.delegate = self;
    self.maxPagesPickerView.dataSource = self;
    
    
    self.timePicker.delegate = self;
    self.timePicker.dataSource = self;
    
    
    self.maxPagesPickerView.interitemSpacing = self.view.frame.size.width / 20;
    self.maxPagesPickerView.textColor = [UIColor colorWithHexString:@"E8E8E8"];
    self.maxPagesPickerView.highlightedTextColor = [UIColor colorWithHexString:@"FFFFFF"];
    
    
    self.timePicker.interitemSpacing = self.view.frame.size.width / 20;

    
    NSString *path = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Make.json"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:path])
    {
        NSArray *design = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:path] options:kNilOptions error:nil];
        makeArray = [[NSMutableArray alloc] initWithArray:design];
        
    }
    self.makePickerView.tag = 1;
    self.makePickerView.delegate = self;
    self.makePickerView.dataSource = self;
    
    NSString *path1 = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Years.json"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:path1])
    {
        NSArray *design = [NSJSONSerialization JSONObjectWithData:[NSData dataWithContentsOfFile:path1] options:kNilOptions error:nil];
        yearsArray = [[NSMutableArray alloc] initWithArray:design];
        
    }
    
    
    self.modelPickerView.tag = 2;
    self.modelPickerView.delegate = self;
    self.modelPickerView.dataSource = self;
    
    
    
    self.startYear.tag = 3;
    self.startYear.delegate = self;
    self.startYear.dataSource = self;
    
    
    
    
    self.endYear.tag = 4;
    self.endYear.delegate = self;
    self.endYear.dataSource = self;
    
    self.carSearchBar.delegate = self;
    
    
    

    
    
    
    
    
    carSearches = [[NSMutableArray alloc] init];
    
    
    [self addEmptyCar];


    selectedSearch = 0;
    self.carsCollectionView.delegate = self;
    self.carsCollectionView.dataSource = self;
}


-(void)viewDidAppear:(BOOL)animated{
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionBadge | UNAuthorizationOptionSound | UNAuthorizationOptionAlert)
                          completionHandler:^(BOOL granted, NSError * _Nullable error) {
                              if (!error) {
                                  NSLog(@"request authorization succeeded!");

                              }
                              else{
                                  NSLog(@"AUTH ERROR = %@" , error.description);
                              }
                          }];
    
    [self.maxPagesPickerView selectItem:0 animated:YES];
    
    BackgroundTask * bgTask =[[BackgroundTask alloc] init];
    
    //Star the timer
    [bgTask startBackgroundTasks:2 target:self selector:@selector(backgroundCallback:)];
    // where call back  is -(void) backgroundCallback:(id)info
    
    [self getData];
    //mainTimer = [NSTimer scheduledTimerWithTimeInterval:(20.0 / carSearches.count) target:self selector:@selector(getData) userInfo:nil repeats:YES];
    
    
    [self.endYear selectRow:yearsArray.count-1 inComponent:0 animated:YES];
}


-(void) backgroundCallback:(id)info{
    NSLog(@"BACKGROUND HAPPENING");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [Bugsnag notifyError:[NSError errorWithDomain:@"didReceiveMemoryWarning" code:408 userInfo:nil]];

    AudioServicesPlaySystemSound(1315);
    [TSMessage addCustomDesignFromFileWithName:@"AlternativeDesign.json"];
    
    [TSMessage showNotificationInViewController:self title:@"Memory overload!!!" subtitle:@"Please reduce the load by reducing opened apps." type:TSMessageNotificationTypeSuccess duration:TSMessageNotificationDurationAutomatic];
    
    UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
    content.title = [NSString localizedUserNotificationStringForKey:@"Memory overload!!!" arguments:nil];
    content.body = @"Please reduce the load by reducing opened apps.";
    content.sound = [UNNotificationSound defaultSound];
    
    // 4. update application icon badge number
    content.badge = [NSNumber numberWithInteger:([UIApplication sharedApplication].applicationIconBadgeNumber + 1)];
    // Deliver the notification in five seconds.
    
    
    NSString *requestId = [NSString stringWithFormat:@"FiveSecond12345"];
    
    UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:1 repeats:NO];
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:requestId content:content trigger:trigger];
    /// 3. schedule localNotification
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
        if (!error) {
            NSLog(@"add NotificationRequest succeeded!");
        }
    }];
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView.tag == 1) {
        return makeArray.count;
    }
    else if (pickerView.tag == 2){
        return modelArray.count;
    }
    else if (pickerView.tag == 3){
        return yearsArray.count;
    }
    else if (pickerView.tag == 4){
        return yearsArray.count;
    }
    else{
        return 0;
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(nullable UIView *)view {
    
    UILabel* tView = (UILabel*)view;
    if (!tView)
    {
        tView = [[UILabel alloc] init];
        //[tView setFont:self.funsLastingText.font];
        [tView setTextAlignment:NSTextAlignmentCenter];
        tView.numberOfLines=1;
        tView.textColor = [UIColor whiteColor];
    }
    
    if (pickerView.tag == 1) {
        NSError *error;
        Make *make = [[Make alloc] initWithDictionary:makeArray[row] error:&error];
        if (error) {
            NSLog(@"ERROR = %@" , error.description);
        }
        else{
            tView.text = make.label;
        }
    }
    else if (pickerView.tag == 2){
        NSError *error;
        Year *year = [[Year alloc] initWithDictionary:modelArray[row] error:&error];
        if (error) {
            NSLog(@"ERROR = %@" , error.description);
        }
        else{
            tView.text = year.name;
        }
    }
    else if (pickerView.tag == 3){
        NSError *error;
        Year *year = [[Year alloc] initWithDictionary:yearsArray[row] error:&error];
        if (error) {
            NSLog(@"ERROR = %@" , error.description);
        }
        else{
            tView.text = year.name;
        }
    }
    else if (pickerView.tag == 4){
        NSError *error;
        Year *year = [[Year alloc] initWithDictionary:yearsArray[row] error:&error];
        if (error) {
            NSLog(@"ERROR = %@" , error.description);
        }
        else{
            tView.text = year.name;
        }
    }
    return tView;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if (pickerView.tag == 1) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.modelLoderView setHidden:NO];
        });
        NSError *error;
        Make *make = [[Make alloc] initWithDictionary:makeArray[row] error:&error];

        
        NSString *str = [NSString stringWithFormat:@"https://www.autotrader.com/rest/searchform/advanced/update?makeCodeList=%@" , make.value];
        
        NSLog(@"SELECTED MODEL = %@" , str);
        
        [Utility getAutoTraderDateFromUrl:str AndResponseBlock:^(id object, BOOL status, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.modelLoderView setHidden:YES];
            });
            if (error) {
                NSLog(@"ERROR AT PICKER REST = %@",error.description);
            }
            else{
                NSDictionary *searchFormFieldsDict = [object objectForKey:@"searchFormFields"];
                NSDictionary *modelDict = [searchFormFieldsDict objectForKey:@"model"];
                NSArray *searchOptions = [modelDict objectForKey:@"searchOptions"];

                if(searchOptions.count){
                    NSDictionary *searchOptionsDict = [searchOptions firstObject];
                    NSArray *optionsArray = [searchOptionsDict objectForKey:@"options"];

                    modelArray = [[NSMutableArray alloc] initWithArray:optionsArray];
                    NSLog(@"Models Gotten= %lu",modelArray.count);
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.modelPickerView reloadAllComponents];
                    });
                }
                
            }
        } AndProgressBlock:^(NSProgress *progress) {
            
        }];
    }
}






- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    CarSearch *carSearch = [carSearches objectAtIndex:selectedSearch];

    return carSearch.allOriginalSearchedCars.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MainTableViewCell"];
    CarSearch *carSearch = [carSearches objectAtIndex:selectedSearch];

    
    if (carSearch.allOriginalSearchedCars.count > indexPath.row) {
        CarModel *car = [carSearch.allOriginalSearchedCars objectAtIndex:indexPath.row];
        
        
        
        cell.titleText.text = car.title;
        cell.trimText.text = car.vin;
        
        cell.makeText.text = car.makeCode;
        cell.modelText.text = car.model;
        cell.phonetext.text = car.ownerPhone;
        cell.emailText.text = car.ownerName;
        
        cell.priceTxt.text = car.salePrice;
        cell.mileAgeTxt.text = car.maxMileage;
        
        
        
        
        
        
        
        //NSLog(@"NEWLY LISTED = %@" , car.newlyListed);
        
        if ([car.isAlreadyOpened isEqual:@(1)] ) {
            //cell.titleText.textColor = [UIColor lightGrayColor];
            cell.topIndicatorView.backgroundColor = [UIColor lightGrayColor];
            cell.bottomIndicatorView.backgroundColor = [UIColor lightGrayColor];
            
            
            
        }
        else{
            if ([car.isVeryNew intValue] > 1) {
                cell.titleText.textColor = [UIColor redColor];
                car.isVeryNew = @([car.isVeryNew intValue]-1);
                [carSearch.allOriginalSearchedCars replaceObjectAtIndex:indexPath.row withObject:car];
                
                cell.topIndicatorView.backgroundColor = [UIColor redColor];
                cell.bottomIndicatorView.backgroundColor = [UIColor redColor];
                
                
            }
            else{
                cell.titleText.textColor = [UIColor darkTextColor];
                
                cell.topIndicatorView.backgroundColor = [UIColor colorWithHexString:@"66C0FD"];
                cell.bottomIndicatorView.backgroundColor = [UIColor colorWithHexString:@"66C0FD"];
                
            }
        }
        
        if ([car.newlyListed isEqual:@(1)]) {
            cell.newlyListedText.hidden = NO;
        }
        else{
            cell.newlyListedText.hidden = YES;
        }
        
        
        
        
        
        cell.backgroundColor = [UIColor clearColor];
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CarSearch *carSearch = [carSearches objectAtIndex:selectedSearch];

    CarModel *car = [carSearch.allOriginalSearchedCars objectAtIndex:indexPath.row];
    car.isAlreadyOpened = @(1);
    [self performSegueWithIdentifier:@"ToDetail" sender:car];

    NSLog(@"URL = %@" , car.vdpSeoUrl);

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [tableView reloadData];
}



- (NSUInteger)numberOfItemsInPickerView:(AKPickerView *)pickerView{
    if (pickerView.tag == 1) {
        return timesArray.count;
    }
    else{
        return maxPagesArray.count;
    }
}

- (NSString *)pickerView:(AKPickerView *)pickerView titleForItem:(NSInteger)item{
    if (pickerView.tag == 1) {
        return timesArray[item];
    }
    else{
        return maxPagesArray[item];

    }
}

- (void)pickerView:(AKPickerView *)pickerView didSelectItem:(NSInteger)item{
    if (pickerView.tag == 1) {
        NSString *selectedTimeStr = timesArray[item];
        selectedTimeStr = [selectedTimeStr stringByReplacingOccurrencesOfString:@" secs" withString:@".0"];
        selectedTime = [selectedTimeStr floatValue];
    }
    else{
        
    }
    
    
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return carSearches.count+1;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CarsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CarsCollectionViewCell" forIndexPath:indexPath];
    
    
    if(indexPath.row < carSearches.count){
        cell.addView.hidden = YES;
        CarSearch *newCarSearch = [carSearches objectAtIndex:indexPath.row];
        cell.makeTxt.text = newCarSearch.carMake;
        cell.modelTxt.text = newCarSearch.carModel;
        cell.allTxt.text = newCarSearch.totalMatches;


        int newCars = 0;
        
        for (CarModel *car in newCarSearch.allOriginalSearchedCars) {
            if ([car.isVeryNew intValue] > 1) {
                newCars = newCars + 1;
            }
        }
        
        
        
       
        
        
        
        // Manually set your badge value to whatever number you want.
        
        
    }
    else{
        cell.addView.hidden = NO;
    }
    
    
    if (indexPath.row == selectedSearch) {
        cell.backBorderView.backgroundColor = [UIColor colorWithHexString:@"4A90E2"];
    }
    else{
        cell.backBorderView.backgroundColor = [UIColor colorWithHexString:@"072B4F"];
    }
    
    
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row < carSearches.count){
        selectedSearch = (int)indexPath.row;
    }else{
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Under Development"
                                     message:@"This Feature is disabled for current build."
                                     preferredStyle:UIAlertControllerStyleAlert];
      
        
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle no, thanks button
                                   }];
        
        //Add your buttons to alert controller
        
        [alert addAction:noButton];
        
        [self presentViewController:alert animated:YES completion:nil];
        
        //[self addEmptyCar];
//        [mainTimer invalidate];
//        mainTimer = nil;
//        mainTimer = [NSTimer scheduledTimerWithTimeInterval:(20.0 / carSearches.count) target:self selector:@selector(getData) userInfo:nil repeats:YES];
    }
    
    
    
    
    [collectionView reloadData];
}


-(void)addEmptyCar {
    CarSearch *newCarSearch = [[CarSearch alloc] init];
    newCarSearch.carMake = @"Acura";
    newCarSearch.carMakeCode = @"ACURA";
    newCarSearch.carModel = @"CL";
    newCarSearch.carModelCode = @"ACUCL";
    newCarSearch.startYear = @"1981";
    newCarSearch.endYear = @"2019";
    newCarSearch.totalMatches = @"...";
    newCarSearch.allOriginalSearchedCars = [[NSMutableArray alloc] init];
    [carSearches addObject:newCarSearch];
}


-(void)getData{
    
    @try {
        [self.mainProgressView setProgress:0.0];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            CarSearch *carSearch = [carSearches objectAtIndex:currentSearchingIndex];
            
            NSLog(@"CALL FOR INDEX = %i" , currentSearchingIndex);
            
            
            if (carSearch.allOriginalSearchedCars.count == 0) {
                carSearch.isFirstTime = YES;
            }
            
            NSString *str = [NSString stringWithFormat:@"https://www.autotrader.com/rest/searchresults/sunset/base?zip=10001&startYear=1981&numRecords=100&sortBy=relevance&firstRecord=0&endYear=2019&searchRadius=0"];
            
            
            str = [NSString stringWithFormat:@"https://www.autotrader.com/rest/searchresults/base?zip=10001&startYear=%@&numRecords=100&sortBy=relevance&firstRecord=0&endYear=%@&modelCodeList=%@&makeCodeList=%@&searchRadius=0" , carSearch.startYear , carSearch.endYear , carSearch.carModelCode , carSearch.carMakeCode];
            
            
            
            
            NSMutableArray *searchURLs = [NSMutableArray arrayWithObjects:
                                          str,nil];
            
            __block int index = 1000;
            
            if ([carSearch.totalMatches intValue]) {
                NSString *selectedMaxStr = maxPagesArray[self.maxPagesPickerView.selectedItem];
                
                int max = [selectedMaxStr intValue];
                
                if ([carSearch.totalMatches intValue] < max) {
                    index = [carSearch.totalMatches intValue];
                }
                else{
                    index = max;
                }
                NSLog(@" %i" , index);
                NSLog(@"SENGING>>>>>>>>>>>>>>>>>>>>>>>>>>");
            }
            
            
            for (int i = 100; i < index ; i=i+100) {
                
                NSString *oldStr = [NSString stringWithFormat:@"firstRecord=%i" , (i-100)];
                NSString *newStr = [NSString stringWithFormat:@"firstRecord=%i" , i];
                
                str = [str stringByReplacingOccurrencesOfString:oldStr withString:newStr];
                [searchURLs addObject:str];
                
            }
            NSLog(@"CALLED URL = %@" , [searchURLs firstObject]);
            __block int checker = 0;
            for (NSString *urls in searchURLs) {
                checker = checker + 1;
                
                NSURL *url = [[NSURL alloc] initWithString:urls];
                NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
                [self.mainWebView loadRequest:request];
                
                
                
                [Utility getAutoTraderDateFromUrl:urls AndResponseBlock:^(id object, BOOL status, NSError *error) {
                    NSLog(@"RETURN FOR INDEX = %i" , currentSearchingIndex);
                    
                    if (error) {
                        NSLog(@"ERROR GOTTEN = %@" , error.description);
                        //[SANotificationView showSAStatusBarBannerWithMessage:@"Check VPN, Internet" backgroundColor:[UIColor redColor] textColor:[UIColor whiteColor  ] showTime:selectedTime];
                    }
                    else{
                        [SANotificationView removeStatusView];
                        NSArray *listingsArrayDict = [object objectForKey:@"listings"];
                        if(listingsArrayDict == nil) {
                            NSLog(@"JSON GOTTEN = %@" , object);
                        }
                        
                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                            //self.totalText.text = [NSString stringWithFormat:@"Total Matches = %@", [object objectForKey:@"totalListingCount"]];
                            
                            
                            CarSearch *carSearch = [carSearches objectAtIndex:currentSearchingIndex];
                            
                            if (![[object objectForKey:@"totalListingCount"] isEqualToString:carSearch.totalMatches]) {
                                carSearch.totalMatches = [object objectForKey:@"totalListingCount"];
                                
                                [carSearches replaceObjectAtIndex:currentSearchingIndex withObject:carSearch];
                                [self.carsCollectionView reloadData];
                            }
                            
                        });
                        
                        CarSearch *carSearch = [carSearches objectAtIndex:currentSearchingIndex];
                        NSLog(@"LISTINGS FOR %@ = %lu" , carSearch.carModel, (unsigned long)listingsArrayDict.count);
                        
                        float errorCheckerIndexer = 0.1f;
                        
                        for (NSMutableDictionary *dict in listingsArrayDict) {
                            NSError *errorParse;
                            
                            
                            
                            //NSString *jsonDummy = @" {\"accelerate\": true, \"certifiedTiles\": [ { \"epn\": \"CERT\", \"image\": { \"src\": \"//images.autotrader.com/cms/images/na/certified/cpo/248500.jpg\" }, \"link\": { \"href\": \"https://ad.doubleclick.net/ddm/trackclk/N3016.autotrader.comSD8257/B20573483.211919735;dc_trk_aid=410931956;dc_trk_cid=80562171;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=\", \"label\": \"Ford Certified Pre-Owned\", \"offsite\": false }, \"thirdPartyImpression\": \"https://ad.doubleclick.net/ddm/trackimp/N3016.autotrader.comSD8257/B20573483.211919735;dc_trk_aid=410931953;dc_trk_cid=80561045;ord=@@ord@@1540828507516\", \"tileType\": \"CERTIFIED_CAR\" } ], \"description\": { \"hasMore\": true, \"label\": \"1 OWNER, CLEAN CARFAX HISTORY, FORD CERTIFIED, F-150 XLT, 4D SuperCrew, 5.0L V8, 10-Speed Automatic, 4WD, ABS brakes, Brake assist, Bumpers:...\" }, \"hasSpecialOffer\": false, \"id\": \"abc\", \"images\": { \"hasVideoIcon\": false, \"primary\": 0, \"sources\": [ { \"alt\": \"Certified 2018 Ford F150 XLT - 492036801\",\"src\": \"https://images.autotrader.com/hn/c/17a3f331506a41058b1a057668735aa4.jpg\",\"title\": \"Certified 2018 Ford F150 XLT\" } ] }, \"isHot\": false, \"isReducedPrice\": true, \"type\": \"Certified\", \"makeCode\": \"FORD\", \"make\": \"Ford\", \"model\": \"F150\", \"owner\": { \"dealerDiff\": {}, \"distanceFromSearch\": 25.1, \"email\": \"groutsos@levittownford.com\", \"id\": \"abc\", \"name\": \"Levittown Ford\", \"phone\": { \"linkText\": \"Get this seller's phone number\", \"privateNumber\": false, \"value\": \"18552914896\", \"isVisible\": true }, \"privateSeller\": false }, \"pricingDetail\": { \"assetType\": \"INCLUDED\", \"callForPricing\": false, \"ctaLink\": false, \"derived\": \"$32,436\", \"first\": \"$32,436\", \"incentive\": 32436, \"msrp\": 0, \"noPriceLabel\": \"Contact Dealer For Price\", \"payments\": { \"label\": \"Est. Finance Payment\", \"paymentDisclaimer\": \"Estimated payment based on 4.97% APR for 72 months with $4,865 down for well qualified buyers. Terms may vary.\", \"value\": 463 }, \"primary\": 32436, \"salePrice\": 32436, \"specialOffers\": 0 }, \"priority\": \"Premium\", \"productTiles\": [ { \"epn\": \"carfaxVHR\", \"image\": { \"src\": \"/content/web/images/na/partner-tiles/255819.gif\" }, \"link\": { \"href\": \"http://www.carfax.com/cfm/ccc_DisplayHistoryRpt.cfm?car_id=492036801&make=FORD&dealer_id=66756167&VIN=-108|-90|81|-87|18|-2|56|116|13|-15|-16|123|-29|63|109|36|-67|83|-64|83|53|-116|-45|89|&partner=ATD_X\", \"label\": \"View the Free CARFAX Report\", \"offsite\": true }, \"tileType\": \"CARFAX_APPENDED\" } ], \"specifications\": { \"interiorColor\": { \"label\": \"Interior Color\", \"value\": \"Black\" }, \"transmission\": { \"label\": \"Transmission\", \"value\": \"Automatic\" }, \"mpg\": { \"label\": \"MPG\", \"value\": \"18 City / 23 Highway\" }, \"engine\": { \"label\": \"Engine\", \"value\": \"8-Cylinder\" }, \"driveType\": { \"label\": \"Drive Type\", \"value\": \"4 wheel drive - rear\" }, \"mileage\": { \"label\": \"mile(s)\", \"value\": \"13,540\" } }, \"style\": [ \"Truck\" ], \"title\": \"Certified 2018 Ford F150 XLT\", \"vin\": \"1FTEW1E52JFB86914\",  \"website\": { \"href\": \"/cars-for-sale/vehicledetails.xhtml?listingId=492036801&zip=10001&referrer=%2Fcars-for-sale%2Fsearchresults.xhtml%3Fzip%3D10001%26startYear%3D1981%26numRecords%3D100%26sortBy%3Drelevance%26incremental%3Dwithin_day%26firstRecord%3D0%26endYear%3D2019%26modelCodeList%3DF150PICKUP%26makeCodeList%3DFORD%26searchRadius%3D0&startYear=1981&numRecords=100&firstRecord=0&endYear=2019&modelCodeList=F150PICKUP&makeCodeList=FORD&searchRadius=0&makeCode1=FORD&modelCode1=F150PICKUP&digitalRetail=true&clickType=listing\" }, \"year\": 2018}";
                            
                            
                            /*NSData *data = [jsonDummy dataUsingEncoding:NSUTF8StringEncoding];
                             NSMutableDictionary* dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
                             */
                            
                            
                            
                            
                            
                            if(![dict objectForKey:@"id"]){
                                int r = arc4random() % 10000;
                                [dict setObject:[NSNumber numberWithInt:r] forKey:@"id"];
                            }
                            
                            
                            
                            if([dict objectForKey:@"id"] == [NSNull null]){
                                int r = arc4random() % 10000;
                                [dict setObject:[NSNumber numberWithInt:r] forKey:@"id"];
                            }
                            
                            if([[dict objectForKey:@"id"] isEqual:[NSNull null]]){
                                int r = arc4random() % 10000;
                                [dict setObject:[NSNumber numberWithInt:r] forKey:@"id"];
                            }
                            
                            if([[dict objectForKey:@"id"] isKindOfClass:[NSNull class]]){
                                int r = arc4random() % 10000;
                                [dict setObject:[NSNumber numberWithInt:r] forKey:@"id"];
                            }
                            
                            if(![[dict objectForKey:@"id"] isKindOfClass:[NSNumber class]]){
                                int r = arc4random() % 10000;
                                [dict setObject:[NSNumber numberWithInt:r] forKey:@"id"];
                            }
                            
                            //dict[@"isReducedPrice"] = [NSNumber numberWithInt:0];
                            
                            CarModel *car = [[CarModel alloc] initWithDictionary:dict error:&errorParse];
                            
                            car.isReducedPriceDummy = [NSNumber numberWithBool:NO];
                            
                            if (errorParse) {
                                NSLog(@"ERROR ON PARSE = %@", errorParse.description);
                            }
                            else{
                                
                                if (!car.id) {
                                    int r = arc4random() % 10000;
                                    car.id = [NSNumber numberWithInt:r];
                                }
                                if([car.id isKindOfClass:[NSNull class]]){
                                    int r = arc4random() % 10000;
                                    car.id = [NSNumber numberWithInt:r];
                                }
                                
                                
                                
                                
//                                if (!car.isReducedPrice) {
//                                    car.isReducedPrice = [NSNumber numberWithInt:0];
//                                }
//                                if([car.isReducedPrice isKindOfClass:[NSNull class]]){
//                                    car.isReducedPrice = [NSNumber numberWithInt:0];
//                                }
                                
                                if (!car.title) {
                                    car.title = @"NO TITLE";
                                }
                                if([car.title isKindOfClass:[NSNull class]]){
                                    car.title = @"NO TITLE";
                                }
                                
                                
                                
                                
                                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.id =[c] %@ ", car.id];
                                
                                
                                NSArray *filteredArray = [carSearch.allOriginalSearchedCars filteredArrayUsingPredicate:predicate];
                                
                                
                                
                                if (filteredArray.count > 0) {
                                    CarModel *previousCar = [filteredArray firstObject];
                                    
                                    if([previousCar.isReducedPriceDummy isEqualToNumber:car.isReducedPriceDummy]){}
                                    else{
                                        if([car.isReducedPriceDummy isEqualToNumber:@(0)]){
                                            car.isVeryNew = @(15);
                                            
                                            [carSearch.allOriginalSearchedCars replaceObjectAtIndex:[carSearch.allOriginalSearchedCars indexOfObject:previousCar] withObject:car.isReducedPriceDummy];
                                            
                                            AudioServicesPlaySystemSound(1315);
                                            [TSMessage addCustomDesignFromFileWithName:@"AlternativeDesign.json"];
                                            
                                            [TSMessage showNotificationInViewController:self title:car.title subtitle:car.title type:TSMessageNotificationTypeSuccess duration:TSMessageNotificationDurationAutomatic];
                                            
                                            UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
                                            content.title = [NSString localizedUserNotificationStringForKey:@"Previous Car Price Reduced." arguments:nil];
                                            content.body = [NSString localizedUserNotificationStringForKey:car.title  arguments:nil];
                                            content.sound = [UNNotificationSound defaultSound];
                                            
                                            // 4. update application icon badge number
                                            content.badge = [NSNumber numberWithInteger:([UIApplication sharedApplication].applicationIconBadgeNumber + 1)];
                                            // Deliver the notification in five seconds.
                                            
                                            
                                            NSString *requestId = [NSString stringWithFormat:@"FiveSecond%f", errorCheckerIndexer];
                                            
                                            UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:errorCheckerIndexer repeats:NO];
                                            UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:requestId content:content trigger:trigger];
                                            /// 3. schedule localNotification
                                            UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
                                            [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
                                                if (!error) {
                                                    NSLog(@"add NotificationRequest succeeded!");
                                                }
                                            }];
                                            errorCheckerIndexer = errorCheckerIndexer + 0.3;
                                        }
                                    }
                                }
                                
                                
                                //NSArray *filteredArray = [allOriginalSearchedCars filteredArrayUsingPredicate:predicate];
                                
                                if (filteredArray.count == 0) {
                                    NSLog(@"NEW FOUND" );
                                    NSDictionary *ownerDict = [dict valueForKey:@"owner"];
                                    if(ownerDict) {
                                        /*
                                         if(![[ownerDict objectForKey:@"privateSeller"] isEqual:[NSNull null]]){
                                         if ([[ownerDict valueForKey:@"privateSeller"] boolValue] == true) {
                                         car.ownerName = @"Private Seller";
                                         }
                                         }
                                         
                                         else{
                                         car.ownerName = [ownerDict valueForKey:@"name"];
                                         car.contactEmail = [ownerDict valueForKey:@"email"];
                                         NSDictionary *phoneDict = [ownerDict valueForKey:@"phone"];
                                         if (phoneDict) {
                                         car.ownerPhone = [phoneDict valueForKey:@"value"];
                                         }
                                         }*/
                                    }
                                    if (![[dict valueForKey:@"pricingDetail"] isKindOfClass:[NSNull class]]){
                                        
                                    }
                                    
                                    
                                    
                                    if (![[dict valueForKey:@"pricingDetail"] isKindOfClass:[NSNull class]]){
                                        NSDictionary *pricingDetailDict = [dict valueForKey:@"pricingDetail"];
                                        if(pricingDetailDict) {
                                            if (![[pricingDetailDict valueForKey:@"salePrice"] isKindOfClass:[NSNull class]]){
                                                car.salePrice = [NSString stringWithFormat:@"%@", [pricingDetailDict valueForKey:@"salePrice"]];
                                            }
                                        }
                                    }
                                    
                                    if (![[dict valueForKey:@"specifications"] isKindOfClass:[NSNull class]]){
                                        NSDictionary *specificationsDict = [dict valueForKey:@"specifications"];
                                        if(specificationsDict) {
                                            if (![[specificationsDict valueForKey:@"mileage"] isKindOfClass:[NSNull class]]){
                                                NSDictionary *mileageDict = [specificationsDict valueForKey:@"mileage"];
                                                if (![[mileageDict valueForKey:@"value"] isKindOfClass:[NSNull class]]){
                                                    car.maxMileage = [NSString stringWithFormat:@"%@" , [mileageDict valueForKey:@"value"]];
                                                    
                                                }
                                            }
                                        }
                                    }
                                    
                                    
                                    if (![[dict valueForKey:@"website"] isKindOfClass:[NSNull class]]){
                                        NSDictionary *websiteDict = [dict valueForKey:@"website"];
                                        if(websiteDict) {
                                            car.vdpSeoUrl = [NSString stringWithFormat:@"%@" , [websiteDict valueForKey:@"href"]];
                                        }
                                    }
                                    
                                    
                                    
                                    
                                    
                                    
                                    if (!carSearch.isFirstTime) {
                                        
                                        if (!carSearch.isFirstTimeAfterTotal) {
                                            
                                            if (1 == 1) {
                                                
                                                AudioServicesPlaySystemSound(1315);
                                                car.isVeryNew = @(15);
                                                
                                                [TSMessage addCustomDesignFromFileWithName:@"AlternativeDesign.json"];
                                                
                                                [TSMessage showNotificationInViewController:self title:car.title subtitle:car.title type:TSMessageNotificationTypeSuccess duration:TSMessageNotificationDurationAutomatic];
                                                
                                                UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
                                                content.title = [NSString localizedUserNotificationStringForKey:@"New Car Found" arguments:nil];
                                                content.body = [NSString localizedUserNotificationStringForKey:car.title  arguments:nil];
                                                content.sound = [UNNotificationSound defaultSound];
                                                
                                                // 4. update application icon badge number
                                                content.badge = [NSNumber numberWithInteger:([UIApplication sharedApplication].applicationIconBadgeNumber + 1)];
                                                // Deliver the notification in five seconds.
                                                
                                                
                                                NSString *requestId = [NSString stringWithFormat:@"FiveSecond%f", errorCheckerIndexer];
                                                
                                                UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:errorCheckerIndexer repeats:NO];
                                                UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:requestId content:content trigger:trigger];
                                                /// 3. schedule localNotification
                                                UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
                                                [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
                                                    if (!error) {
                                                        NSLog(@"add NotificationRequest succeeded!");
                                                    }
                                                }];
                                                errorCheckerIndexer = errorCheckerIndexer + 0.3;
                                            }
                                        }
                                        
                                        
                                    }
                                    [carSearch.allOriginalSearchedCars addObject:car];
                                }
                            }
                        }
                        
                    }
                    checker = checker - 1;
                    dispatch_async(dispatch_get_main_queue(), ^(void) {
                        [self.mainProgressView setProgress:self.mainProgressView.progress+0.1 animated:YES];
                    });
                    
                    
                    
                    CarSearch *carSearch = [carSearches objectAtIndex:currentSearchingIndex];
                    if (checker == 0) {
                        if (error) {
                            if(specialErrorChecker == 5) {
                                //[SANotificationView showSAStatusBarBannerWithMessage:@"Check VPN, Internet" backgroundColor:[UIColor redColor] textColor:[UIColor whiteColor  ] showTime:1.0];
                                
                                UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
                                content.title = [NSString localizedUserNotificationStringForKey:@"Error" arguments:nil];
                                content.body = [NSString localizedUserNotificationStringForKey:@"Please Check your Internet and VPN."  arguments:nil];
                                content.sound = [UNNotificationSound defaultSound];
                                
                                // 4. update application icon badge number
                                // Deliver the notification in five seconds.
                                UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:0.1f repeats:NO];
                                UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"ErrorNotify" content:content trigger:trigger];
                                /// 3. schedule localNotification
                                UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
                                [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
                                    if (!error) {
                                        NSLog(@"ErrorNotify succeeded!");
                                    }
                                }];
                            }
                            specialErrorChecker = specialErrorChecker + 1;
                        }
                        else{
                            specialErrorChecker = 0;
                        }
                        NSLog(@"ALL LOADED FOR %@ = %lu",carSearch.carModel , (unsigned long)carSearch.allOriginalSearchedCars.count);
                        /*carSearch.allOriginalSearchedCars = [[carSearch.allOriginalSearchedCars sortedArrayUsingComparator:^NSComparisonResult(CarModel *a, CarModel *b) {
                         return a.listingId<b.listingId;
                         }] mutableCopy];
                         carSearch.allOriginalSearchedCars = [[carSearch.allOriginalSearchedCars sortedArrayUsingComparator:^NSComparisonResult(CarModel *a, CarModel *b) {
                         return a.newlyListed<b.newlyListed;
                         }] mutableCopy];
                         carSearch.allOriginalSearchedCars = [[carSearch.allOriginalSearchedCars sortedArrayUsingComparator:^NSComparisonResult(CarModel *a, CarModel *b) {
                         return a.isReducedPrice<b.isReducedPrice;
                         }] mutableCopy];
                         */
                        
                        
                        
                        carSearch.allOriginalSearchedCars = [[carSearch.allOriginalSearchedCars sortedArrayUsingComparator:^NSComparisonResult(CarModel *a, CarModel *b) {
                            return [a.isVeryNew intValue] < [b.isVeryNew intValue];
                        }] mutableCopy];
                        
                        
                        
                        
                        
                        
                        dispatch_async(dispatch_get_main_queue(), ^(void) {
                            [self.mainTableView reloadData];
                        });
                        
                        if ((currentSearchingIndex+1) == carSearches.count) {
                            currentSearchingIndex = 0;
                        }
                        else{
                            currentSearchingIndex = currentSearchingIndex + 1;
                        }
                        
                        
                        /*
                         for (CarModel *car in allOriginalSearchedCars) {
                         NSLog(@"PHONE %@ , %@" , car.ownerPhone , car.title);
                         }
                         */
                        
                        if (carSearch.isFirstTime) {
                            NSString *totalStr = [object objectForKey:@"totalListingCount"];
                            
                            totalStr = [totalStr stringByReplacingOccurrencesOfString:@"," withString:@""];
                            
                            total = [totalStr intValue];
                            carSearch.haveTotal = YES;
                        }
                        else{
                            carSearch.isFirstTimeAfterTotal = NO;
                        }
                        
                        carSearch.isFirstTime = NO;
                        
                        
                        NSLog(@"TIMER = %f" , (selectedTime / carSearches.count));
                        
                        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)((selectedTime / carSearches.count) * NSEC_PER_SEC));
                        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                            
                            [self getData];
                        });
                        
                        
                    }
                } AndProgressBlock:^(NSProgress *progress) {
                    
                }];
            }
        });
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception.reason);
    }
    @finally {
        NSLog(@"Finally condition");
    }
    
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"ToDetail"]) {
        DetailViewController *vc = segue.destinationViewController;
        vc.selectedCar = (CarModel *)sender;
    }
}

- (IBAction)buttonAction:(UIButton *)sender {
    //
    sender.enabled = NO;
    [self getData];
}

- (IBAction)sliderButtonAction:(UIButton *)sender {

    
    if (self.bottomConstraint.constant == 0) {
        self.bottomConstraint.constant = 298;
        
//        if (mainTimer) {
//            [mainTimer invalidate];
//            mainTimer = nil;
//        }
    }
    else{
        self.bottomConstraint.constant = 0;
//        if (!mainTimer) {
//            mainTimer = [NSTimer scheduledTimerWithTimeInterval:(20.0 / carSearches.count) target:self selector:@selector(getData) userInfo:nil repeats:YES];
//        }
    }
    
    
    [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self.view layoutIfNeeded];
    } completion:nil];
    
}

- (IBAction)dragAction:(UIButton *)sender {
    [self sliderButtonAction:sender];
}

- (IBAction)setAction:(UIButton *)sender {
    
    
    //[[Crashlytics sharedInstance] crash];


    
    
    //NSArray *crashArray = [[NSArray alloc] init];

    //id objectHJK = crashArray[0];
    
    
    if (modelArray.count == 0) {
        return;
    }
    self.totalText.text = [NSString stringWithFormat:@"Total Matches = %@", @"..."];

    selectedMake = [[Make alloc] initWithDictionary:[makeArray objectAtIndex:[self.makePickerView selectedRowInComponent:0]] error:nil];
    selectedModel = [[Year alloc] initWithDictionary:[modelArray objectAtIndex:[self.modelPickerView selectedRowInComponent:0]] error:nil];
    selectedStart = [[Year alloc] initWithDictionary:[yearsArray objectAtIndex:[self.startYear selectedRowInComponent:0]] error:nil];
    selectedEnd = [[Year alloc] initWithDictionary:[yearsArray objectAtIndex:[self.endYear selectedRowInComponent:0]] error:nil];

    
    
    
    
    
    
    haveTotal = NO;

   

    
    
    CarSearch *carSearch = [carSearches objectAtIndex:selectedSearch];
    
    carSearch.carMake = selectedMake.label;
    carSearch.carMakeCode = selectedMake.value;
    carSearch.carModel = selectedModel.name;
    carSearch.carModelCode = selectedModel.value;
    carSearch.startYear = selectedStart.value;
    carSearch.endYear = selectedEnd.value;
    carSearch.totalMatches = @"...";
    carSearch.isFirstTime = YES;
    
    carSearch.isFirstTimeAfterTotal = YES;
    carSearch.allOriginalSearchedCars = [[NSMutableArray alloc] init];
    
    [carSearches replaceObjectAtIndex:selectedSearch withObject:carSearch];
    
    
    [self.carsCollectionView reloadData];
    [self.mainTableView reloadData];
    [self getData];
    
    
    
    NSDictionary *parameters = @{ @"carMake": [NSString stringWithFormat:@"%@",selectedMake.label],
                                  @"carModel": [NSString stringWithFormat:@"%@",selectedModel.name],
                                  @"carYears": [NSString stringWithFormat:@"%@-%@",selectedStart.value,selectedEnd.value],
                                  @"carSite": [NSString stringWithFormat:@"%@",@"AutoTrader"] };
    
    [Utility saveCarWithDictionary:parameters AndResponseBlock:^(id object, BOOL status, NSError *error) {
        if(error){
            NSLog(@"ERROR = %@" , error.description);
        }
    }];
    
    
    
    
    
    
    
    

//    [mainTimer invalidate];
//    mainTimer = nil;
//    mainTimer = [NSTimer scheduledTimerWithTimeInterval:(20.0 / carSearches.count) target:self selector:@selector(getData) userInfo:nil repeats:YES];
//
    
}
@end
